package noc

import (
	"reflect"

	"gitlab.com/akita/akita"
)

type TrafficCounter struct {
	TotalData uint64
}

func (c *TrafficCounter) Type() reflect.Type {
	return reflect.TypeOf((akita.Req)(nil))
}

func (c *TrafficCounter) Pos() akita.HookPos {
	return akita.ConnDeliverHookPos
}

func (c *TrafficCounter) Func(item interface{}, domain akita.Hookable, info interface{}) {
	req := item.(akita.Req)
	c.TotalData += uint64(req.ByteSize())
}
