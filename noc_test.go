package noc

import (
	"log"
	"reflect"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
)

func TestNOC(t *testing.T) {
	log.SetOutput(GinkgoWriter)
	RegisterFailHandler(Fail)
	RunSpecs(t, "GCN3")
}

type sendRecvAgent struct {
	akita.ComponentBase
	ticker *akita.Ticker

	out akita.Port

	sendLeft             int
	sendCount, recvCount int
}

func (a *sendRecvAgent) Handle(e akita.Event) error {
	switch e.(type) {
	case akita.TickEvent:
		req := akita.NewReqBase()
		req.SetSendTime(e.Time())
		req.SetSrc(a.out)
		req.SetDst(a.out)
		req.SetByteSize(64)

		err := a.out.Send(req)
		if err == nil {
			a.sendLeft--
			a.sendCount++
		}

		if a.sendLeft > 0 {
			a.ticker.TickLater(e.Time())
		}
	default:
		log.Panicf("cannot process request of type %s", reflect.TypeOf(e))
	}
	return nil
}

func (a *sendRecvAgent) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
}

func (a *sendRecvAgent) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	port.Retrieve(now)
	a.recvCount++
}

func newSendRecvAgent(name string, engine akita.Engine) *sendRecvAgent {
	agent := new(sendRecvAgent)
	agent.ComponentBase = *akita.NewComponentBase(name)

	agent.ticker = akita.NewTicker(agent, engine, 1*akita.GHz)
	agent.out = akita.NewLimitNumReqPort(agent, 4096)

	return agent
}
